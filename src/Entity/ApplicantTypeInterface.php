<?php

namespace Drupal\appointments\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining name type entities.
 */
interface ApplicantTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
